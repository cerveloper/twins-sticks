﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaySystem : MonoBehaviour {

    private const int bufferFrames = 100;
    private MyKeyFrame[] keyFrame = new MyKeyFrame[bufferFrames];
    private Rigidbody rig;
    private Manager manager;

	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody>();
        manager = FindObjectOfType<Manager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (manager.recording)
        {
            Record();
        }
        else {
            PlayBack();
        }

    }

    private void Record()
    {
        rig.isKinematic = false;
        int frame = Time.frameCount % bufferFrames;
        keyFrame[frame] = new MyKeyFrame(Time.time, transform.position, transform.rotation);
    }

    private void PlayBack() {
        rig.isKinematic = true;
        int frame = Time.frameCount % bufferFrames;
        transform.position = keyFrame[frame].framePos;
        transform.rotation = keyFrame[frame].frameRot;

    }
}

/// <summary>
/// A structure to save The values of a ball
/// </summary>
public struct MyKeyFrame {
    public float frameTime;
    public Vector3 framePos;
    public Quaternion frameRot;

    public MyKeyFrame(float aTime, Vector3 aPosition, Quaternion aRotation) {
        frameTime = aTime;
        framePos = aPosition;
        frameRot = aRotation;
    }
}
